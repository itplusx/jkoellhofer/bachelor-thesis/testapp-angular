import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Address} from '../address';
import {AddressService} from "../address.service";

@Component({
    selector: 'app-address',
    templateUrl: './address.component.html',
    styleUrls: ['./address.component.css']
})
export class AddressComponent {

    @Input() address: Address;
    @Output() onDelete: EventEmitter<Address> = new EventEmitter();

    constructor(private addressService: AddressService) {
    }

    delete() {
        this.onDelete.emit(this.address);
    }
}