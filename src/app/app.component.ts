import {Component, OnInit} from '@angular/core';
import {AddressService} from "./address.service";
import {Address} from './address';


@Component({
    selector: 'app',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

    ngOnInit() {
        this.getAddresses();
    }

    title = 'Fictional Address List: Angular';
    addresses: Address[];

    constructor(private addressService: AddressService) {
    }

    getAddresses() {
        this.addressService.getAddresses()
            .subscribe(
                (addresses: Address[]) => this.addresses = addresses,
                (error: Response) => console.log(error)
            );
    }

    createAddress(address: Address) {
        this.addressService.createAddress(address)
            .subscribe(
                () => this.getAddresses(),
                (error: Response) => console.log(error)
            );
    }

    deleteAddress(address: Address) {
        this.addressService.deleteAddress(address)
            .subscribe(
                () => this.getAddresses(),
                (error: Response) => console.log(error)
            );
    }
}
