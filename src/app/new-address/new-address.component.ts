import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AddressService} from "../address.service";
import {NgForm} from "@angular/forms";
import {Address} from "../address";

@Component({
    selector: 'app-new-address',
    templateUrl: './new-address.component.html',
    styleUrls: ['./new-address.component.css']
})
export class NewAddressComponent {

    @Output() onCreate: EventEmitter<Address> = new EventEmitter();

    constructor(private addressService: AddressService) {
    }

    submit(form: NgForm) {
        console.log(form.value);  // { first: '', last: '' }
        console.log(form.valid);  // false
        this.onCreate.emit(form.value);
        form.reset();
    }
}
