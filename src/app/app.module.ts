import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AddressComponent } from './address/address.component';
import { AddressListComponent } from './address-list/address-list.component';
import { NewAddressComponent } from './new-address/new-address.component';
import { AddressService } from "./address.service";

@NgModule({
  declarations: [
    AppComponent,
    AddressComponent,
    AddressListComponent,
    NewAddressComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [AddressService],
  bootstrap: [AppComponent]
})
export class AppModule { }
