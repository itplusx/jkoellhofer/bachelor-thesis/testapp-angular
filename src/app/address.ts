export class Address {
    id: number;
    firstName: string;
    lastName: string;
    addressLine: string;
    city: string;
    postCode: string;
    state: string;
}