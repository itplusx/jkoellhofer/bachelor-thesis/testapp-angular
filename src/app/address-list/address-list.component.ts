import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Address} from "../address";
import {AddressService} from "../address.service";

@Component({
    selector: 'app-address-list',
    templateUrl: './address-list.component.html',
    styleUrls: ['./address-list.component.css']
})
export class AddressListComponent {

    @Input() addresses: Address[];
    @Output() onDelete: EventEmitter<Address> = new EventEmitter();

    constructor(private addressService: AddressService) {
    }

    onDeleteAddress(address: Address) {
        this.onDelete.emit(address);
    }
}
