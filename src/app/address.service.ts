import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/Rx'
import {Address} from "./address";

@Injectable()
export class AddressService {

    constructor(private http: Http) {
    }

    getAddresses() {
        return this.http.get('http://testapp-api.dev/api/addresses')
            .map(
                (response: Response) => {
                    return response.json().addresses;
                }
            );
    }

    deleteAddress(address: Address) {
        return this.http.delete('http://testapp-api.dev/api/addresses/' + address.id);
    }

    createAddress(address: Address) {
        const body = JSON.stringify(
            address
        );
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.post('http://testapp-api.dev/api/addresses', body, {headers: headers});
    }
}
