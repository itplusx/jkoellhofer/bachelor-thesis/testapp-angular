import { TestappAngularPage } from './app.po';

describe('testapp-angular App', () => {
  let page: TestappAngularPage;

  beforeEach(() => {
    page = new TestappAngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
